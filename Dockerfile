# Build the manager binary
FROM golang:1.15

WORKDIR /workspace
# Copy the Go Modules manifests
COPY . .

# cache deps before building and copying source so that we don't need to re-download as much
# and so that source changes don't invalidate our downloaded layer
RUN go mod tidy
RUN chmod +x run.sh
EXPOSE 8000

ENTRYPOINT ["./run.sh"]
