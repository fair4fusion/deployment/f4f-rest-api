# Get Access Token

Access http://rest-operator.fair4fusion.iit.demokritos.gr/get_token
either while previously logged in Gitlab.com or sign in using
**gitlab-f4f** option.

You will be redirected to a page that contains a json. Find the
`access_token` key and copy its value without the enclosing quotes.

Set the environment variable `access_token` to this value.


# API endpoints

### `/addQuery -X POST -F fileName=@<fileName> -H "Authorization: Bearer $access_token"`

Besides access to the data stored in the infrastructure, pipelines
also have access to a user-provided "query". Whether and how this will
be interpreted and used by each pipeline, depends on the pipeline
definition. The API makes the file uploaded by this function available
by mountinga in the pipeline containers, as the `/shared` directory, a
volume containing all files uploaded using this function.


### `/deploy -X POST -F myFile=@<fileName> -H "Authorization: Bearer $access_token" `

Deploy an experiment in the cluster. The uploaded file must be a YAML
file following the format of example_pipeline.yaml.


### `/status -H "Authorization: Bearer $access_token"`

Get the status of all pods that have been instantiated by the f4f operator.


### `/status_all -H "Authorization: Bearer $access_token"`

Get the status of all pods in the cluster.


### `/show?arg=<path> -H "Authorization: Bearer $access_token"  `

Shows a directory listing of the volume where pipelines store their
final results, that is, the outputs of the final step in the pipeline.


### `/fetch?arg=<pathname> -H "Authorization: Bearer $access_token"  `

Download a file from the volume where pipelines store their final
results, that is, the outputs of the final step in the pipeline.
